#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  gcruft.pl
#
#        USAGE:  ./gcruft.pl > cruftfiles 
#
#  DESCRIPTION:  helps finding orphan files on a gentoo linux system
#
# REQUIREMENTS:  gentoo ;)
#       AUTHOR:  Tobias Hommel (insecure), software@genoetigt.de
#      VERSION:  0.1.1
#      CREATED:  08/09/2009 11:25:22 PM
#===============================================================================

use strict;
#use warnings;use diagnostics;

use lib '/etc/gcruft';
use config;

our $progname='gcruft';
our $version='0.1.1';

require '/usr/share/gcruft/functions.pl';

our $cfdir = '/etc/gcruft';

# == colours ==
our $reset="\e[0m";
our $bold="\e[1m";
our $red="\e[1;31m";
our $green="\e[1;32m";
our $yellow="\e[1;33m";
our $blue="\e[1;34m";
our $magenta="\e[1;35m";
our $darkcyan="\e[0;36m";
our $white="\e[0;37m";


my %whitelistdirs = ();
our %packagefiles = ();
my %files = ();
our %installedpackages = ();
my %installedexceptions = ();
our @wldirs = ();
our @wlfiles = ();
our $pn = '';
our @pvs = ();

# == internal functions ==
sub findwalk {
	my $cdir = shift;
	my $entry;
	my @cdirs;
	my $ce;
	opendir DIR, $cdir;
	(@cdirs=readdir(DIR)) && closedir(DIR);
	foreach $entry(@cdirs) {
		next if $entry =~ /^\.{1,2}$/;
		$ce="$cdir/$entry";
		next if exists($whitelistdirs{$ce});
		$files{$ce} = '' unless exists($packagefiles{$ce});
		findwalk($ce) if -d $ce && ! -l $ce;
	}
}

sub apply_lib_mapping {
	my $pf;
	my @k=keys(%packagefiles);
	foreach $pf(@k) {
		if ($pf =~ m|^(/usr)?/lib/(.*)$|) {
			delete $packagefiles{$pf};
			if ($1) {
				$packagefiles{"$1/$libmap/$2"} = '';
			} else {
				$packagefiles{"/$libmap/$2"} = '';
			}
		}
	}
}

# == MAIN ==

print STDERR "${bold}finding files contained in packages...$reset\n";
my $centry;
my $pentry;
my $i;
my $j;
my @centries;
my @pentries;
opendir CDIR, $installedbase;
@centries = readdir(CDIR);
closedir CDIR;
foreach $centry(@centries) {
	next if $centry =~ /^\.{1,2}$/s;
	opendir PDIR, "$installedbase/$centry";
	@pentries = readdir(PDIR);
	closedir PDIR;
	foreach $pentry(@pentries) {
		next if $pentry =~ /^\.{1,2}$/s;
		open CFILE, "$installedbase/$centry/$pentry/CONTENTS";
		while ($j=<CFILE>) {
			if ($j=~/^obj (.*) .+ .+\n$/s) {
				$i=$1;
				# add object
				$packagefiles{$i} = '';
				# if .py, add compiled files
				if ($i=~/\.py$/s) {
					$packagefiles{$i.'c'} = '';
					$packagefiles{$i.'o'} = '';
				}
				next;
			} elsif ($j=~/^dir (.*)\n$/s) {
				# add object
				$packagefiles{$1} = '';
				next;
			} elsif ($j=~/^sym (.*) ->/s) {
				$i=$1;
				# add object
				$packagefiles{$i} = '';
				# if .py, add compiled files
				if ($i=~/\.py$/s) {
					$packagefiles{$i.'c'} = '';
					$packagefiles{$i.'o'} = '';
				}
				next;
			}
		}
		close CFILE;
		$pentry =~ /^(.+)-([0-9].*)$/;
		unshift @{$installedpackages{"$centry/$1"}}, $2; 
		print STDERR " $yellow* $reset$centry/$pentry\n";
	}
}

if ($libmap ne 'lib') {
	print STDERR "${bold}applying libmapping...$reset\n";
	apply_lib_mapping();
}

print STDERR "${bold}applying general exceptions...$reset\n";
foreach $pn(glob '/usr/share/gcruft/exceptions/*.pl') {
	$pn =~ /.*\/(.+?).pl$/;
	$installedexceptions{$1} = $pn;
}
foreach $pn(glob "$cfdir/exceptions/*.pl") {
	$pn =~ /.*\/(.+?).pl$/;
	$installedexceptions{$1} = $pn;
}
foreach $pn(sort keys %installedexceptions) {
	print STDERR " $yellow* $darkcyan$pn$reset\n";
	@wldirs=();
	@wlfiles=();
	require $installedexceptions{$pn};
	@whitelistdirs{@wldirs} = ();
	@packagefiles{@wlfiles} = ();
}

print STDERR "${bold}applying package specific exceptions...$reset\n";
foreach $pn(glob '/usr/share/gcruft/exceptions/*/*.pl') {
	$pn =~ /.*\/(.+?\/.+?).pl$/;
	$installedexceptions{$1} = $pn;
}
foreach $pn(glob "$cfdir/exceptions/*/*.pl") {
	$pn =~ /.*\/(.+?\/.+?).pl$/;
	$installedexceptions{$1} = $pn;
}
foreach $pn(sort keys %installedexceptions) {
	next unless exists($installedpackages{$pn});
	print STDERR " $yellow* $darkcyan$pn$reset\n";
	@wldirs=();
	@wlfiles=();
	@pvs=@{$installedpackages{$pn}};
	require $installedexceptions{$pn};
	@whitelistdirs{@wldirs} = ();
	@packagefiles{@wlfiles} = ();
}

print STDERR "${bold}finding files on system...$reset\n";
foreach (@checkdirs) {
	next unless -d $_ && ! -l $_;
	print STDERR " $yellow* $reset$_\n";
#	$files{$_} = '' unless exists($packagefiles{"$_"});
	findwalk($_);
}

print map { "$_\n" } sort keys %files;
