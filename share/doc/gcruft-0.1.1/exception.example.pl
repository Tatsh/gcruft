# =============================================================================
# general rules:
#  * directories always without trailing '/'
#  * only define local variables (local $myvar;) or be sure, what you're doing
#  * always use /$libmap instead of '/lib' and /usr/$libmap instead of '/usr/lib'
#  * every variable used in config.pm is usable in an exception module
#  * when developing a new exception module, you can use debugprint() for
#    debugging purposes
#  * local exceptions (from /etc/gcruft/exceptions) override those distributed
#    with gcruft (from /usr/lib/gcruft/exceptions), unless inherited(see below)
# =============================================================================

# =======================================
# == exclude one or more directories   ==
# =======================================
push @wldirs, '/var/run/ConsoleKit','/var/log/ConsoleKit';

# =======================================
# == exclude a file                    ==
# =======================================
push @wlfiles, '/var/run/sshd.pid';

# =======================================
# == exclude more than one file        ==
# =======================================
push @wlfiles, split /\n/, <<FILES;
/usr/bin/c++
/usr/bin/cc
/usr/bin/cpp
/usr/bin/g++
/usr/bin/gcc
/usr/bin/cpp
/usr/bin/$CHOST-c++
/usr/bin/$CHOST-cpp
/usr/bin/$CHOST-g++
/usr/bin/$CHOST-gcc
/usr/bin/$CHOST-gcj
/usr/bin/$CHOST-gcjh
/usr/bin/$CHOST-gfortran
FILES

# =======================================
# == use $libmap                       ==
# =======================================
push @wlfiles, "/usr/$libmap/libc-client.so.1";

# =======================================
# == use file globbing                 ==
# =======================================
push @wlfiles, glob('/usr/bin/c{89,99}');

# =======================================
# == use $man_ext                      ==
# =======================================
push @wlfiles, '/usr/bin/ctags', glob "/usr/share/man/man1/ctags.1$man_ext";

# =======================================
# == use gen_pathlist()                ==
# =======================================
push @wlfiles, &gen_pathlist('/usr/local/bin/command');
# expands to:
push @wlfiles, '/usr/local/bin/command', '/usr/local/bin', '/usr/local', '/usr';

# =======================================
# == apply version specific exceptions ==
# == uses category/name-version.pl     ==
# =======================================
foreach (@pvs) {
	&apply_version_exception("$pn-$_");
}

# =======================================
# == apply slot specific exceptions    ==
# == uses category/name:slot.pl        ==
# =======================================
foreach (@pvs) {
	&apply_slot_exception("$pn-$_");
}

# =======================================
# == inherit the distro exception      ==
# =======================================
require "/usr/share/gcruft/exceptions/$pn.pl";

# =======================================
# == always return successfully        ==
# =======================================
1;
