foreach (glob '/var/run/vservers/*') {
	if (m|.*/([^/]+)$|) {
		push @wlfiles, "/var/lock/vserver.etcvservers$1.startup";
	}
}
push @wldirs, '/etc/vservers','/var/run/vservers','/var/run/vservers.rev','/var/run/vshelper';
push @wlfiles, '/var/lock/vserver.interfaces','/var/lock/vservers/vservers-default';
1;
