#actually it belongs to qt4-build.eclass, but it seems that all ebuilds
#inheriting this eclass also depend directly or indirectly on qt-core
push @wlfiles, '/usr/share/qt4/mkspecs/qconfig.pri';
foreach (glob '/usr/include/qt4/Gentoo/gentoo-*-qconfig.h') {
	if (exists($packagefiles{$_})) {
		push @wlfiles, '/usr/include/qt4/Gentoo/gentoo-qconfig.h';
		last;
	}
}
1;
