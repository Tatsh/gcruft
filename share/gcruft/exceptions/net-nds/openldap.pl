push @wldirs, '/etc/openldap/ssl';
open SLAPDC, '/etc/openldap/slapd.conf';
while (<SLAPDC>) {
	if (/(argsfile|pidfile|include|TLSCACertificateFile|TLSCertificateFile|TLSCertificateKeyFile)\s+([^\s]+)/) {
		push @wlfiles, $2;
	}
	if (/directory\s+([^\s]+)/) {
		push @wldirs, $1;
	}
}
close SLAPDC;
1;
