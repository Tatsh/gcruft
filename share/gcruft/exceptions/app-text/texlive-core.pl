push @wlfiles, split /\n/,<<PLAIN;
/etc/texmf/web2c/fmtutil.cnf
/etc/texmf/web2c/texmf.cnf
/etc/texmf/web2c/updmap.cfg
/var/lib/texmf/fonts
/var/lib/texmf/fonts/map
/var/lib/texmf/fonts/map/dvipdfm
/var/lib/texmf/fonts/map/dvipdfm/updmap
/var/lib/texmf/fonts/map/dvipdfm/updmap/dvipdfm.map
/var/lib/texmf/fonts/map/dvipdfm/updmap/dvipdfm_dl14.map
/var/lib/texmf/fonts/map/dvipdfm/updmap/dvipdfm_ndl14.map
/var/lib/texmf/fonts/map/dvips
/var/lib/texmf/fonts/map/dvips/updmap
/var/lib/texmf/fonts/map/dvips/updmap/builtin35.map
/var/lib/texmf/fonts/map/dvips/updmap/download35.map
/var/lib/texmf/fonts/map/dvips/updmap/ps2pk.map
/var/lib/texmf/fonts/map/dvips/updmap/psfonts.map
/var/lib/texmf/fonts/map/dvips/updmap/psfonts_pk.map
/var/lib/texmf/fonts/map/dvips/updmap/psfonts_t1.map
/var/lib/texmf/fonts/map/pdftex
/var/lib/texmf/fonts/map/pdftex/updmap
/var/lib/texmf/fonts/map/pdftex/updmap/pdftex.map
/var/lib/texmf/fonts/map/pdftex/updmap/pdftex_dl14.map
/var/lib/texmf/fonts/map/pdftex/updmap/pdftex_ndl14.map
/var/lib/texmf/web2c/updmap.log
PLAIN

open MKTEXLSR, 'kpsewhich --show-path=ls-R|';
local $lsrdirs = <MKTEXLSR>;
close MKTEXLSR;
chomp $lsrdirs;
foreach (split /:/, $lsrdirs) {
	s|//|/|g;
	push @wlfiles, "$_/ls-R";
}

1;
