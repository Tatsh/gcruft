open SLC, '/etc/syslog-ng/syslog-ng.conf';
while (<SLC>) {
	while (s/.*?file\("(.*?)"\)(.*)/$2/g) {
		push @wlfiles, $1;
		&rotatelog($1);
	}
}
close SLC;
push @wlfiles, '/var/run/syslog-ng.pid', '/var/run/syslog-ng.ctl', '/var/lib/syslog-ng.persist';
1;
