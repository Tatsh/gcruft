open ENV, '/etc/env.d/03opengl';
local $implementation = '';
while (<ENV>) {
	if (/OPENGL_PROFILE="(.*)"/) {
		$implementation = $1;
		last;
	}
}
close ENV;
foreach (glob "/usr/$libmap/opengl/$implementation/lib/libGL{,core}.{so,dylib,a}") {
	m|/(libGL[^/]+)$|;
	push @wlfiles, "/usr/$libmap/$1";
}
foreach (glob "/usr/$libmap/opengl/$implementation/lib/tls/libGL{,core}.{so,dylib,a}") {
	m|/(libGL[^/]+)$|;
	push @wlfiles, "/usr/$libmap/tls/$1";
}
foreach (glob "/usr/$libmap/opengl/$implementation/extensions/*.{so,dylib,a}") {
	m|/([^/]+)$|;
	push @wlfiles, "/usr/$libmap/xorg/modules/extensions/$1";
}
push @wlfiles, glob "/usr/include/GL/{gl,glx,glxtokens,glext,glxext,glxmd,glxproto}.h";
push @wlfiles, '/etc/env.d/03opengl';
1;
