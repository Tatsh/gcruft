open CF, '/etc/conf.d/sssh';
while (<CF>) {
	next unless /^(config|pid)_(.+)="(.+)"/;
	push @wlfiles, $3;
	if ($1 eq 'config') {
		push @wlfiles, "/etc/init.d/sssh.$2";
		open ICF, $3;
		while (<ICF>) {
			if (/^\$logfile\s*=\s*(['"])(.+)\1/) {
				push @wlfiles, $2;
				last;
			}
		}
		close ICF;
	}
}
close CF;

1;
