push @wlfiles, glob '/usr/bin/{emacs,emacsclient,b2m,ebrowse,rcs-checkin,grep-changelog,etags}';
push @wlfiles, glob "/usr/share/man/man1/{emacs,emacsclient,b2m,rcs-checkin,grep-changelog,ebrowse,gfdl,etags}.1$man_ext";
push @wlfiles, '/etc/env.d/50emacs';
1;
