# does not resolve variables, so things like btree:$data_directory/verify_cache won't be detected
open POSTCF, 'LC_ALL=C /usr/sbin/postconf|';
while (<POSTCF>) {
	s/^.* = (.*)$/$1/;
	foreach (split /, /, $_) {
		if (/^hash:(.+)/) {
			push @wlfiles, $1, "$1.db";
		} elsif (m|^(/.+)|) { # also matches things like /var/spool/postfix, but that's ok, even better ;)
			push @wlfiles, $1;
		}
	}
}
close POSTCF;
push @wldirs, '/var/spool/postfix', '/etc/ssl/postfix';
push @wlfiles, '/var/lib/postfix/master.lock';
1;
