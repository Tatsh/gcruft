my $ldir = '';
open LAYCONF, '/etc/layman/layman.cfg' or die "couldn't open layman.cfg: $@";
while (<LAYCONF>) {
	if (/^storage\s*:\s*(.*)$/) {
		$ldir = $1;
		last;
	}
}
close LAYCONF;
push @wldirs, $ldir;
1;
