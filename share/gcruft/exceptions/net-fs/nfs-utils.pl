push @wldirs, '/var/lib/nfs/sm', '/var/lib/nfs/sm.bak', '/var/lib/nfs/rpc_pipefs','/var/lib/nfs/v4recovery','/var/lib/nfs/v4root';
push @wlfiles, split /\n/, <<FILES;
/var/lib/nfs
/var/lib/nfs/etab
/var/lib/nfs/.etab.lock
/var/lib/nfs/rmtab
/var/lib/nfs/state
/var/lib/nfs/xtab
/var/lib/nfs/.xtab.lock
/var/run/rpc.statd.pid
/var/run/sm-notify.pid
FILES
1;
