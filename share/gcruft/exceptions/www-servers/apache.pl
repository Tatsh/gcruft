foreach (glob "/etc/apache2/{vhosts.d/*.conf,httpd.conf}") {
	open CF, $_;
	while (<CF>) {
		if (/(SSLCertificateFile|SSLCertificateKeyFile)\s+([^\s]*)/) {
			push @wlfiles, $2;
		}
	}
	close CF;
}
push @wlfiles, '/etc/ssl/apache2', glob '/etc/ssl/apache2/server.{key,csr,crt,pem}';
push @wldirs, '/etc/apache2','/var/log/apache2';
1;
