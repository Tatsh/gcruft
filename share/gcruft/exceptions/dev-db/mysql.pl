open MYCNF, '/etc/mysql/my.cnf';
while (<MYCNF>) {
	if (/(log-error|err-log)\s*=\s*([^\s]*)/) {
		push @wlfiles, $2;
		&rotatelog($2);
	}
	if (/pid-file\s*=\s*([^\s]*)/) {
		push @wlfiles, $1;
	}
}
close MYCNF;
push @wlfiles, '/var/log/mysql/mysql.log';
1;
