local $i = '';
open ENV, "/etc/env.d/gcc/config-$CHOST";
while (<ENV>) {
	if (/CURRENT=(.*)/) {
		$i="/etc/env.d/gcc/$1";
		$1 =~ /^$CHOST-(...).*$/;
		push @wlfiles, "/usr/$libmap/pkgconfig/libgcj-$1.pc";
		last;
	}
}
close ENV;
local $gcc_path = '';
if ($i ne '') {
	open ENV, $i;
	while (<ENV>) {
		if (/GCC_PATH="(.*)"/) {
			$gcc_path = $1;
			last;
		}
	}
}
close ENV;
if ($gcc_path) {
	foreach (glob "$gcc_path/{cpp,cc,gcc,c++,g++,f77,g77,gcj,gcjh,gcov,gdc,gdmd,gfortran}") {
		m|([^/]+)$|;
		push @wlfiles, "/usr/bin/$1","/usr/bin/$CHOST-$1";
	}
}
push @wlfiles, "/etc/env.d/05gcc-$CHOST", "/$libmap/cpp", "/etc/env.d/gcc/config-$CHOST", '/etc/env.d/gcc/.NATIVE',"/usr/$libmap/pkgconfig/libgcj.pc";
1;
