push @wldirs, '/etc/env.d/binutils';
local $pv = $pvs[0];
$pv =~ s/^(.*)-r.*/$1/;
foreach (glob "/usr/$CHOST/binutils-bin/$pv/*") {
	m|/([^/]+)$|;
	push @wlfiles, "/usr/bin/$1", "/usr/bin/$CHOST-$1","/usr/$CHOST/bin/$1",;
}
foreach (glob "/usr/$libmap/binutils/$CHOST/$pv/include/*.h") {
	m|/([^/]+)$|;
	push @wlfiles, "/usr/include/$1";
}
foreach (glob "/usr/$libmap/binutils/$CHOST/$pv/*.{a,so,la}") {
	m|/([^/]+)$|;
	push @wlfiles, "/usr/$CHOST/lib/$1";
}
push @wlfiles, '/etc/env.d/05binutils', "/usr/$CHOST/lib/ldscripts", "/usr/$CHOST/bin","/usr/$CHOST/lib";
1;
