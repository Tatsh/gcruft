open KVER, '/proc/version';
my $kver = <KVER>;
close KVER;
chomp $kver;
$kver =~ s/^Linux version (.*?) .*$/$1/;
push @wldirs, "/$libmap/modules/$kver";

push @wldirs
	, $PORTDIR
	, '/var/db'
	, $installedbase
	, '/var/cache/edb'
	, '/etc/runlevels'
	, @PORTDIR_OVERLAY
	#TODO do we really want to whitelist those dirs?
	, $DISTDIR
	, $PKGDIR
	, $PORTAGE_TMPDIR
	, '/etc/config-archive';

foreach (keys %installedpackages) {
	if (/^games-/) {
		push @wlfiles, '/etc/env.d/90games';
		last;
	}
}

# created by elisp-site-regen from elisp-common.eclass
# but easier for now
if (exists($packagefiles{'/usr/share/emacs/site-lisp/site-gentoo.d'})) {
	push @wlfiles, '/usr/share/emacs/site-lisp/site-gentoo.el';
}

# created by update_vim_afterscripts from vim-plugin.eclass
# but easier for now
foreach (glob '/usr/share/vim/vimfiles/after/{,*/}*.d') {
	if (exists($packagefiles{$_})) {
		/^(.*)\.d$/;
		push @wlfiles, $1;
	}
}

# == fonts fonts fonts ==
&fontdir('/usr/share/fonts');

# == some general logfiles, with their respective logrotated versions ==
foreach ('/var/log/dmesg','/var/log/lastlog','/var/log/wtmp') {
	&rotatelog($_);
}

# == info directories, dir-files ==
open PROFILEENV, '/etc/profile.env';
while (<PROFILEENV>) {
	if (/^export INFO(PATH|DIR)='(.*)'$/) {
		local $infodirs = $2;
		$infodirs =~ tr/:/,/;
		push @wlfiles, glob "{$infodirs}/dir";
	}
}

# == lost+found for mountpoints ==
open MOUNTS, '/proc/mounts';
while (<MOUNTS>) {
	if (/^[^ ]+ ([^ ]+) ([^ ]+) /) {
		if ($2 !~ /^proc|sysfs|devtmpfs|devpts|selinuxfs|usbfs|binfmt_misc|fusectl$/) {
			push @wlfiles, "$1/lost+found";
		}
	}
}
close MOUNTS;

# == gnome2_icon_cache_update from gnome2-utils.eclass generates icon-theme.cache files ==
foreach (glob '/usr/share/icons/*/index.theme') {
	if (exists($packagefiles{$_})) {
		m|^(.*)/index.theme|;
		push @wlfiles, "$1/icon-theme.cache";
	}
}

push @wlfiles, split /\n/, <<FILES;
/etc/fstab
/etc/.pwd.lock
/etc/group
/etc/group-
/etc/gshadow
/etc/gshadow-
/etc/hostname
/etc/hosts
/etc/localtime
/etc/make.conf
/etc/make.profile
/etc/motd
/etc/mtab
/etc/passwd
/etc/passwd-
/etc/resolv.conf
/etc/shadow
/etc/shadow-
/usr/tmp
/var/log/dmesg
/var/log/lastlog
/var/log/wtmp
/var/run/utmp
/etc/timezone
FILES
1;
