# actually the plugins-*.dat name is generated somehow like
# plugins-XXYYZZ-CPU.dat, where XX=sizeof(int), YY=sizeof(void *), ZZ=1e?,
# CPU=combination of some CPU flags (see src/moduls/cache.c from vlc package)
push @wlfiles, glob "/usr/$libmap/vlc/plugins/plugins-*-*.dat";
1;
