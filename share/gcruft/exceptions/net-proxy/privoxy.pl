local $logdir;
local $logfile;
open PC, '/etc/privoxy/config';
while (<PC>) {
	if (/^logdir[ \t]+(.*?)\/*$/) {
		$logdir = $1;
	}
	if (/^logfile[ \t]+(.*)$/) {
		$logfile = $1;
	}
}
close PC;
&rotatelog("$logdir/$logfile");
push @wlfiles, "$logdir/$logfile", '/var/run/privoxy.pid';
1;
