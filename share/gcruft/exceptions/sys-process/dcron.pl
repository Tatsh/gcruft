push @wldirs, '/var/spool/cron/crontabs', '/var/spool/cron/lastrun';
if ($pvs[0] =~ /^4\.4/) {
	push @wldirs, '/var/spool/cron/cronstamps';
}
&rotatelog('/var/log/cron.log');
push @wlfiles, split /\n/, <<FILES;
/var/log/cron.log
/var/run/cron.pid
FILES
1;
