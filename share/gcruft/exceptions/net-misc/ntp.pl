open NTPC, '/etc/ntp.conf';
while (<NTPC>) {
	if (/^driftfile\s+(.*)$/) {
		push @wlfiles, $1;
	}
}
close NTPC;
push @wlfiles, '/var/run/ntpd.pid';
1;
