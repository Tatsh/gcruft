#TODO the handling of .ph files is slow, maybe speed it up a bit
use Config;

system('rm -rf /tmp/gcruft_temp_perl');
mkdir "/tmp/gcruft_temp_perl";
# no <=perl-5.12 for now, as i'm not sure, why the h2ph calls in perl-cleaner are so strange (without -d)
if ($pvs[0] =~ /^5\.12/) {
	open H2PH, "LC_ALL=C /usr/bin/h2ph -a -d /tmp/gcruft_temp_perl /usr/include/asm/termios.h /usr/include/syscall.h /usr/include/syslimits.h /usr/include/syslog.h /usr/include/sys/ioctl.h /usr/include/sys/socket.h /usr/include/sys/time.h /usr/include/wait.h /usr/include/sysexits.h 2>/dev/null|";
	while (<H2PH>) {
		/.* -> (.*)$/;
		push @wlfiles, "$Config{'archlib'}/$1";
	}
	close H2PH;
}
system('rm -rf /tmp/gcruft_temp_perl');
push @wlfiles, map "$Config{'archlib'}/$_", split /\n/, <<PLAIN;
_h2ph_pre.ph
asm/termios.ph
syscall.ph
syslimits.ph
syslog.ph
sys/ioctl.ph
sys/socket.ph
sys/time.ph
wait.ph
sysexits.ph
asm
asm-generic
bits
gnu
linux
machine
sys
Encode/ConfigLocal.pm
PLAIN

local $dualscripts;
open EBUILD, "$installedbase/$pn-$pvs[0]/perl-$pvs[0].ebuild";
{
	local $/='dual_scripts() {';
	$dualscripts = <EBUILD>;
	local $/='}';
	$dualscripts = <EBUILD>;
}
close EBUILD;
foreach (split /\n/, $dualscripts) {
	next unless /^\s*src_remove_dual_scripts\s+([^\s]+)\s+([^\s]+)\s+(.+)$/;
	foreach (split / /,$3) {
		push @wlfiles, "/usr/share/man/man1/$_.1$man_ext", "/usr/bin/$_";
	}
}

push @wlfiles, "$Config{'archlib'}/perllocal.pod", "$Config{'privlib'}/CPAN/Config.pm";
push @wldirs, $Config{'installsitearch'};
1;
