open GHCPKG, 'LC_ALL=C ghc-pkg list --simple-output --no-user-package-conf --global|';
local @packages=split / /, <GHCPKG>;
close GHCPKG;
local $pname;
foreach $pname(@packages) {
		$pvs[0] =~ /^(.+)(-r.*)?/;
		push @wlfiles, glob "/usr/$libmap/ghc-$1/package.conf.d/$pname-[0-9a-f]*.conf";
}
1;
