push @wldirs, '/etc/portage','/var/lib/portage','/var/log/portage';
&rotatelog('/var/log/emerge.log');
&rotatelog('/var/log/emerge-fetch.log');
push @wlfiles, split /\n/, <<FILES;
/etc/csh.env
/etc/ld.so.cache
/etc/ld.so.conf
/etc/prelink.conf
/etc/profile.env
/var/lib/module-rebuild
/var/lib/module-rebuild/moduledb
/var/log/emerge.log
/var/log/emerge-fetch.log
FILES
1;
