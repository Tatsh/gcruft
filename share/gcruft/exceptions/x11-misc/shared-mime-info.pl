my $usm='/usr/share/mime';
push @wldirs, "$usm/application";

opendir USMPDIR, "$usm/packages";
foreach my $entry(readdir USMPDIR) {
	if (exists($packagefiles{"$usm/packages/$entry"})) {
		open MIMEXML, "$usm/packages/$entry";
		while (<MIMEXML>) {
			if (/<mime-type type="(.*?)">/) { # correct match? is '<mime-type .* type=...' better?
				#		push @wlfiles, "$usm/$1.xml";
				push @wlfiles, &gen_pathlist("$usm/$1.xml");
			}
		}
		close MIMEXML;
	}
}
closedir USMPDIR;
push @wlfiles, split /\n/, <<FILES;
/usr/share/mime/XMLnamespaces
/usr/share/mime/aliases
/usr/share/mime/generic-icons
/usr/share/mime/globs
/usr/share/mime/globs2
/usr/share/mime/icons
/usr/share/mime/magic
/usr/share/mime/mime.cache
/usr/share/mime/subclasses
/usr/share/mime/treemagic
/usr/share/mime/types
FILES
1;
