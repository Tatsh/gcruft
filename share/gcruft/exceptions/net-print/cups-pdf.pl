local $spooldir = '/var/spool/cups-pdf';
local $s;
open CC, '/etc/cups/cups-pdf.conf';
while (<CC>) {
	if (/^Out[ \t]+(.+?)(\${(USER|HOME)}\/?)?$/) {
		if ($1 !~ /\${(USER|HOME)}/) {
			$spooldir = $1;
			$spooldir =~ s/\/$//;
		}
		last; # we only read the first occurence
	}
}
close CC;

push @wldirs, $spooldir;
1;
