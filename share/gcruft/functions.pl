sub fontdir {
	local $dir = shift;
	foreach (glob "$dir/*") {
		if (exists($packagefiles{$_})) {
			if (-d $_) {
				push @wlfiles, glob "$_/{encodings.dir,fonts.alias,fonts.dir,fonts.scale}";
				&fontdir($_);
			}
		}
	}
}

sub pythonverdir {
	if (!defined($pythonverdir)) {
		open PC, '/etc/env.d/python/config';
		our $pythonverdir = <PC>;
		chomp $pythonverdir;
		close PC;
	}
	return $pythonverdir; 
}

sub rotatelog {
	# TODO: improve ;) maybe parse logrotate config to get real extensions...
	my $logfile = shift;
	push @wlfiles, glob "$logfile*.gz";
}

sub vimshare {
	if (!defined($vimshare)) {
		return '' unless defined($installedpackages{'app-editors/vim-core'});
		open VIMC, "$installedbase/app-editors/vim-core-$installedpackages{'app-editors/vim-core'}[0]/CONTENTS";
		our $vimshare = '';
		while (<VIMC>) {
			if (m|^dir (/usr/share/vim/vim[0-9]+)$|) {
				$vimshare = $1;
				last;
			}
		}
		close VIMC;
	}
	return $vimshare;
}

sub debugprint {
	my @str = shift;
	print STDERR " $red>> $green@str$reset\n";
}

sub gen_pathlist {
	my $origpath = shift;
	my %splitlist = ();
	while ($origpath ne '') {
		$splitlist{$origpath} = '';
		$origpath =~ s|^(.*)/.*?$|$1|;
	}
	return keys %splitlist;
}

# -- takes $pf=category/name-version and returns the corresponding slot
sub get_pkg_slot {
	my $pf = shift;
	my $pslot;
	open SLOT, "$installedbase/$pf/SLOT";
	{   
		local $/='';
		$pslot = <SLOT>;
		chomp $pslot;
	}
	close SLOT;
	return $pslot;
}

# -- takes $pf=category/name-version
sub apply_slot_exception {
	my $pf = shift;
	my $pslot = &get_pkg_slot($pf);
	my $ps = $pn.':'.$pslot;
	if (-e "$exceptionsdir/$ps.pl") {                                                                      
		print STDERR "   $yellow- $darkcyan$ps$reset\n";
		@wldirs=();
		@wlfiles=();
		require "$exceptionsdir/$ps.pl";
		push @whitelistdirs, @wldirs;
		@packagefiles{@wlfiles} = ();
	}
}

# -- takes $pf=category/name-version
sub apply_version_exception {
	my $pf = shift;
	if (-e "$exceptionsdir/$pf.pl") {
		print STDERR "   $yellow- $darkcyan$pf$reset\n";
		@wldirs=();
		@wlfiles=();
		require "$exceptionsdir/$pf.pl";
		push @whitelistdirs, @wldirs;
		@packagefiles{@wlfiles} = ();
	}
}

1;
