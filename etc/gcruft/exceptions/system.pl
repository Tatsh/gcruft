use File::Find;

sub add_to_list {
    push @wlfiles, $File::Find::name;
}

push @wlfiles, '/etc/dhcpcd.duid',
               '/etc/rarreg.txt',
               '/etc/init.d/fix-udev-empty',
               '/etc/udev/hwdb.bin',
               '/var/log/dmesg',
               '/etc/fstab',
               '/etc/.gitignore',
               '/var/log/rc.log',
               '/etc/sudoers.d/ansible',
               '/usr/tmp',
               '/usr/sbin/fix_libtool_files.sh',
               '/usr/src/linux',
               '/usr/bin/awk',
               '/var/empty/.keep';

find(\&add_to_list, '/var/lib/alsa');

push @wlfiles, <"/etc/udev/rules.d/99-z-*.rules">;
push @wlfiles, <"/etc/openvpn/*.conf">;
push @wlfiles, <"/etc/group*">;
push @wlfiles, <"/etc/gshadow*">;
push @wlfiles, <"/etc/shadow*">;
push @wlfiles, <"/etc/mtab*">;
push @wlfiles, <"/etc/passwd*">;

find(\&add_to_list, '/etc/runlevels');
find(\&add_to_list, '/var/db/sudo');
find(\&add_to_list, '/var/lib/dhcpcd');

find(\&add_to_list, '/etc/runlevels/boot');
find(\&add_to_list, '/etc/runlevels/default');
find(\&add_to_list, '/etc/runlevels/shutdown');
find(\&add_to_list, '/etc/runlevels/sysinit');

find(\&add_to_list, '/var/lib/ansible');

my $release = `uname -r`;
chomp $release;
find(\&add_to_list, "/usr/src/linux-$release");
find(\&add_to_list, "/lib64/modules/$release");

1;
