# Ansible managed: modified on 2014-08-08 03:02:51

use File::Find;

sub add_to_list {
    push @wlfiles, $File::Find::name;
}

find(\&add_to_list, '/var/tmp/kdecache-root');
find(\&add_to_list, '/var/tmp/kdecache-kdm');
find(\&add_to_list, '/var/tmp/kdecache-tatsh');

push @wlfiles, '/etc/machine-id';

1;
