# Ansible managed: modified on 2014-08-08 02:58:55

use File::Find;

sub add_to_list {
    push @wlfiles, $File::Find::name;
}

push @wlfiles, '/bin/awk';
push @wlfiles, '/etc/conf.d/net';
push @wlfiles, '/usr/bin/x86_64-pc-linux-gnu-gcc-ar';
push @wlfiles, '/usr/bin/x86_64-pc-linux-gnu-gcc-nm';
push @wlfiles, '/usr/bin/x86_64-pc-linux-gnu-gcc-ranlib';

push @wlfiles, <"/etc/gcruft/exceptions/*.pl">;
push @wlfiles, <"/etc/eselect/*/active*">;
push @wlfiles, <"/etc/init.d/net.*">;
push @wlfiles, <"/etc/init.d/openvpn.*">;

find(\&add_to_list, '/etc/.git');

1;
