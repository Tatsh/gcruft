# Ansible managed: modified on 2016-02-26 03:13:51

use File::Find;

sub add_to_list {
    push @wlfiles, $File::Find::name;
}

push @wlfiles, '/usr/lib64/postgresql'; push @wlfiles, '/usr/share/postgresql'; push @wlfiles, '/var/log/postgresql-general.log';
push @wlfiles, <"/etc/postgresql-*/*.conf">;
push @wlfiles, <"/usr/include/pg_*.h /usr/include/postgres*">;
push @wlfiles, <"/etc/env.d/*postgresql">;

push @wlfiles, <"/usr/bin/clusterdb*">;
push @wlfiles, <"/usr/bin/createdb*">;
push @wlfiles, <"/usr/bin/createlang*">;
push @wlfiles, <"/usr/bin/createuser*">;
push @wlfiles, <"/usr/bin/dropdb*">;
push @wlfiles, <"/usr/bin/droplang*">;
push @wlfiles, <"/usr/bin/dropuser*">;
push @wlfiles, <"/usr/bin/ecpg*">;
push @wlfiles, <"/usr/bin/initdb*">;
push @wlfiles, <"/usr/bin/pgbench*">;
push @wlfiles, <"/usr/bin/pg_*">;
push @wlfiles, <"/usr/bin/postgres*">;
push @wlfiles, <"/usr/bin/postmaster*">;
push @wlfiles, <"/usr/bin/psql*">;
push @wlfiles, <"/usr/bin/vacuumdb*">;
push @wlfiles, <"/usr/bin/vacuumlo*">;
push @wlfiles, <"/usr/bin/reindexdb*">;


1;
