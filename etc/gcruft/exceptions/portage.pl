# Ansible managed: modified on 2014-08-08 02:59:01

use File::Find;

sub add_to_list {
    push @wlfiles, $File::Find::name;
}

find(\&add_to_list, '/etc/config-archive');
find(\&add_to_list, '/usr/portage');
find(\&add_to_list, '/var/db/pkg');
find(\&add_to_list, '/var/tmp/portage');
find(\&add_to_list, '/var/cache/edb');
push @wlfiles, '/etc/make.conf';

1;
