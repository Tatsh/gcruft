# Ansible managed: modified on 2014-08-08 02:59:05

package config;
use strict;
use vars qw(@ISA @EXPORT);
require Exporter;
@ISA = qw(Exporter AutoLoader);
@EXPORT = qw(
    @checkdirs @PORTDIR_OVERLAY
    $installedbase $libmap $man_ext $multilib
    $DISTDIR $CHOST $PKGDIR $PORTAGE_TMPDIR $PORTDIR
);

our @checkdirs = split /\n/, <<DIRS;
/bin
/etc
/lib
/lib32
/lib64
/opt
/sbin
/usr
/var
DIRS

our $DISTDIR = '/usr/portage/distfiles';
our $CHOST = 'x86_64-pc-linux-gnu';
our $PKGDIR = '/usr/portage/packages';
our $PORTAGE_TMPDIR = '/var/tmp';
our $PORTDIR = '/usr/portage';

our $installedbase = '/var/db/pkg';
our $multilib = 'yes';
our $libmap = 'lib64';
our $man_ext = '.bz2';

our @PORTDIR_OVERLAY = qw(/var/lib/layman/srcshelton
/var/lib/layman/vortex
);
push @PORTDIR_OVERLAY, qw(/var/lib/layman/jamiel-gentoo-overlay
/var/lib/layman/tatsh-overlay
);

1;
